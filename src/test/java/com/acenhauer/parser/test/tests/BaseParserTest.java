package com.acenhauer.parser.test.tests;

import com.acenhauer.parser.selenium.BaseVanillaAceWebDriver;
import org.apache.commons.lang3.StringEscapeUtils;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by guillem on 18/07/15.
 */
public class BaseParserTest extends BaseVanillaAceWebDriver {

    @Test public void parser() throws Exception {

        String textChannels = "{";

        textChannels += parseFromAceTV("detskie");
        textChannels += parseFromAceTV("film");
        textChannels += parseFromAceTV("man");
        textChannels += parseFromAceTV("music");
        textChannels += parseFromAceTV("news");
        textChannels += parseFromAceTV("obchie");
        textChannels += parseFromAceTV("poznovat");
        textChannels += parseFromAceTV("razvlekat");
        textChannels += parseFromAceTV("region");
        textChannels += parseFromAceTV("sport");
        textChannels += parseFromAceTV("porno");

        textChannels = textChannels.substring(0, textChannels.length() - 1);
        textChannels += "}";
        textChannels =
            StringEscapeUtils.unescapeHtml4(textChannels).replaceAll("[^\\x20-\\x7e]", "");
        textChannels = textChannels.replaceAll("\\s+", " ");
        System.out.println(textChannels);
        PrintWriter out = new PrintWriter("vanilla-ace-channels.txt");
        out.println(textChannels);
        out.close();
    }

    public String createChannelList(HashMap<String, String> channels, String category) {
        String initialCategory = "'" + category + "': [";
        String textChannels = "";
        for (Map.Entry<String, String> entry : channels.entrySet()) {
            String key = entry.getKey().replaceAll("'", "").replaceAll("\\+", "");
            String value = entry.getValue();
            textChannels +=
                "{'name': '" + key + "'," + "'thumb': 'http://acenhauer.com/logos/acestream.png',"
                    + "'video': '" + value + "'," + "'genre': '" + category + "'},";
        }
        if (!textChannels.isEmpty()) {
            textChannels = initialCategory + textChannels;
            textChannels = textChannels.substring(0, textChannels.length() - 1);
            textChannels += "],";
        }
        return textChannels;
    }

    public String parseFromTorrentTVru(String categoryFromWeb, String category) {
        HashMap<String, String> channels = new HashMap<>();
        driver().get("http://torrent-tv.ru/category.php?cat=" + categoryFromWeb);
        wait.pause(5000);
        System.out.println("Current parsing in progress from ..." + driver().getCurrentUrl());
        int numberOfTotalChannels =
            driver().findElements(By.xpath("html/body/div[3]/div[3]/div[1]/div[3]/div")).size();
        for (int p = 1; p <= numberOfTotalChannels; p++) {
            String channelUrl = driver()
                .findElement(By.xpath("html/body/div[3]/div[3]/div[1]/div[3]/div[" + p + "]/h5/a"))
                .getAttribute("href");
            String[] parts = channelUrl.split("=");
            String aceStreamChannelLink = "http://content.asplaylist.net/" + parts[1] + ".acelive";
            String channelName = driver().findElement(
                By.xpath("html/body/div[3]/div[3]/div[1]/div[3]/div[" + p + "]/h5/a/strong"))
                .getText();
            if (!isCyrillicChannel(channelName)) {
                System.out.println("channelName: " + channelName);
                channels.put(channelName, aceStreamChannelLink);
            }
        }
        return createChannelList(channels, category);
    }

    public boolean isCyrillic(char c) {
        return Character.UnicodeBlock.CYRILLIC.equals(Character.UnicodeBlock.of(c));
    }

    public boolean isCyrillicChannel(String channel) {
        char[] a = channel.toCharArray();
        for (char c : a) {
            if (isCyrillic(c)) {
                return true;
            }
        }
        return false;
    }

    public String parseFromAceTV(String category) {
        List<String> channelNames = new ArrayList<String>();
        List<String> aceStreamLinks = new ArrayList<String>();

        HashMap<String, String> channels = new HashMap<>();
        String url = "http://torrent-tv-online.pp.ua/_tv/" + category + "/counter.js";
        StringBuilder response = null;
        System.out.println("Requesting parsing from ..." + url);
        URL website = null;
        try {
            website = new URL(url);
            URLConnection connection = website.openConnection();
            BufferedReader in =
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
            response = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String content[] = response.toString().split("\\[");
        String[] channelsFromParse = content[1].split("\\]");
        String[] channelsParsed = channelsFromParse[0].split("\"");
        for (int i = 1; i < channelsParsed.length; i++) {
            String channelName = "";
            if (i % 2 == 0) {
                channelName = channelsParsed[i].substring(5);
                String[] removedChannels = channelName.split("\\(");
                channelName = removedChannels[0];
                channelNames.add(channelName);
            }
        }
        for (int i = 1; i < channelsParsed.length; i++) {
            String aceStreamLink = "";
            if (i % 2 != 0) {
                aceStreamLink = channelsParsed[i];
                aceStreamLinks.add(aceStreamLink);
            }
        }

        for (int w = 1; w < aceStreamLinks.size(); w++) {
            if (isUTF8MisInterpreted(channelNames.get(w))) {
                System.out.println("Compatible channel name: " + channelNames.get(w));
                System.out.println("Channels AceStream: " + aceStreamLinks.get(w));
                channels.put(channelNames.get(w), aceStreamLinks.get(w));
            }
        }

        return createChannelList(channels, capitalize(getCategoryFromAceTV(category)));
    }

    private String getCategoryFromAceTV(String category) {
        if (category.equals("detskie")) {
            return "kids";
        } else if (category.equals("film")) {
            return "movies";
        } else if (category.equals("man")) {
            return "entertainment";
        } else if (category.equals("music")) {
            return "music";
        } else if (category.equals("news")) {
            return "news";
        } else if (category.equals("sport")) {
            return "sports";
        } else if (category.equals("porno")) {
            return "pr0n";
        }
        return "general";
    }

    public static String capitalize(String s) {
        if (s.length() == 0)
            return s;
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }

    public static boolean isUTF8MisInterpreted(String input) {
        //convenience overload for the most common UTF-8 misinterpretation
        //which is also the case in your question
        return isUTF8MisInterpreted(input, "Windows-1252");
    }

    public static boolean isUTF8MisInterpreted(String input, String encoding) {

        CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
        CharsetEncoder encoder = Charset.forName(encoding).newEncoder();
        ByteBuffer tmp;
        try {
            tmp = encoder.encode(CharBuffer.wrap(input));
        } catch (CharacterCodingException e) {
            return false;
        }

        try {
            decoder.decode(tmp);
            return true;
        } catch (CharacterCodingException e) {
            return false;
        }
    }

}
