package com.acenhauer.parser.test.pages;

import com.testcloud.qasentinel.selenium.RemoteWebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by guillem.hernandez on 18/12/2014.
 */
public class ArenaVisionAgendaPage {

    private final WebDriver driver;
    private final RemoteWebDriverWait wait;

    public ArenaVisionAgendaPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new RemoteWebDriverWait(driver, 2);
    }

    public String getAgenda(WebDriver driver) {
        return driver.findElement(By.xpath(".//*[@id='main']/div/div/div/div/p[2]")).getText();
    }
}
