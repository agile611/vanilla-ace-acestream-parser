package com.acenhauer.parser.test.pages;

import com.testcloud.qasentinel.selenium.BaseWebDriverTest;
import com.testcloud.qasentinel.selenium.RemoteWebDriverWait;
import com.testcloud.qasentinel.utils.NavigationActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by guillem.hernandez on 18/12/2014.
 */
public class ArenaVisionPage {

    private final WebDriver driver;
    private final RemoteWebDriverWait wait;

    public ArenaVisionPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new RemoteWebDriverWait(driver, 2);
    }

    public String getAceStreamLink(WebDriver driver) {
        return driver.findElement(By.xpath(
            ".//*[@id='main']/div/div/div/div/div[3]/table/tbody/tr/td/div/div[2]/p[2]/a[2]"))
            .getAttribute("href").toString();
    }
}
