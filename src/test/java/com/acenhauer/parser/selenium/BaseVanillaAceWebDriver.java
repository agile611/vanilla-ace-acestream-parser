package com.acenhauer.parser.selenium;

import com.testcloud.qasentinel.reporters.BaseTest;
import com.testcloud.qasentinel.saucelabs.*;
import com.testcloud.qasentinel.selenium.BaseWebDriverTest;
import com.testcloud.qasentinel.selenium.BrowserCapabilities;
import com.testcloud.qasentinel.selenium.RemoteWebDriverWait;
import com.testcloud.qasentinel.utils.PropUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Listeners({SauceOnDemandTestListener.class}) public class BaseVanillaAceWebDriver extends BaseTest
    implements SauceOnDemandSessionIdProvider, SauceOnDemandAuthenticationProvider {
    public static final int DRIVER_SELENIUM_TIMEOUT_MILISECONDS = (new Integer(10000)).intValue();
    public static Properties testProperties;
    public static String host;
    public static String hub;
    public static String browserName;
    public static String user;
    public static String passwd;
    public static RemoteWebDriverWait wait;
    public static String startUrl;
    public SauceOnDemandAuthentication authentication;
    private InheritableThreadLocal<WebDriver> globalDriver;
    private InheritableThreadLocal<Logger> globalLogger;
    private InheritableThreadLocal<String> sessionId;
    private InheritableThreadLocal<BrowserCapabilities> globalBrowserCapabilities;

    public BaseVanillaAceWebDriver() {
        this.authentication = new SauceOnDemandAuthentication(SauceHubParser.getUserSaucelabs(hub),
            SauceHubParser.getApikeySaucelabs(hub));
        this.globalDriver = new InheritableThreadLocal();
        this.globalLogger = new InheritableThreadLocal();
        this.sessionId = new InheritableThreadLocal();
        this.globalBrowserCapabilities = new InheritableThreadLocal();
    }

    @BeforeMethod(
        alwaysRun = true) public void setUp(Method method, Object[] testArguments)
        throws IOException {
        this.setTestName(method.getName());
        startUrl = testProperties.getProperty("host");
        this.globalLogger.set(Logger.getLogger(BaseWebDriverTest.class));
        this.globalBrowserCapabilities.set(new BrowserCapabilities());
        RemoteWebDriver rwd = null;

        try {
            DesiredCapabilities e = this.globalBrowserCapabilities.get()
                .getDesiredCapabilitiesPerBrowser(browserName,
                    method.getName() + "-" + UUID.randomUUID());
            rwd = new RemoteWebDriver(new URL(hub), e);
            this.sessionId.set(rwd.getSessionId().toString());
            this.globalLogger.get()
                .info("Starting " + browserName + " using sessionId " + this.getSessionId());
        } catch (MalformedURLException var5) {
            var5.printStackTrace();
        }

        this.globalDriver.set(rwd);
        this.globalDriver.get().manage().timeouts()
            .pageLoadTimeout((long) DRIVER_SELENIUM_TIMEOUT_MILISECONDS, TimeUnit.MILLISECONDS);
        this.globalDriver.get().manage().timeouts()
            .setScriptTimeout((long) DRIVER_SELENIUM_TIMEOUT_MILISECONDS, TimeUnit.MILLISECONDS);
        this.globalDriver.get().manage().timeouts()
            .implicitlyWait((long) DRIVER_SELENIUM_TIMEOUT_MILISECONDS, TimeUnit.MILLISECONDS);
        this.globalDriver.get().manage().deleteAllCookies();
        wait = new RemoteWebDriverWait(this.globalDriver.get(),
            (long) DRIVER_SELENIUM_TIMEOUT_MILISECONDS);
        this.globalDriver.get().manage().window().maximize();
    }

    @AfterMethod(
        alwaysRun = true) protected void teardown(ITestResult tr) {
        this.globalDriver.get().quit();
        if (tr.isSuccess()) {
            this.logger().info(this.getSessionId() + " PASSED! ");
        } else {
            this.logger().info(this.getSessionId() + " FAILED! ");
        }

        this.globalLogger.get().info("Finished execution for testcase " + this.getSessionId());
    }

    protected WebDriver driver() {
        return this.globalDriver.get();
    }

    protected Logger logger() {
        return this.globalLogger.get();
    }

    public String getSessionId() {
        return this.sessionId.get();
    }

    public String getHost() {
        return host;
    }

    public SauceOnDemandAuthentication getAuthentication() {
        return this.authentication;
    }

    static {
        testProperties = PropUtils.getProcessedTestProperties();
        host = testProperties.getProperty("host");
        hub = testProperties.getProperty("hub");
        browserName = testProperties.getProperty("browser");
        user = testProperties.getProperty("user");
        passwd = testProperties.getProperty("passwd");
    }
}
