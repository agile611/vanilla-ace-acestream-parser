package com.acenhauer.parser.helpers;

/**
 * Created by guillem on 15/01/16.
 */

public class ChannelInfo {

    public String channelName;
    public String thumb;
    public String video;
    public String genre;

    public ChannelInfo(String channelName, String thumb, String video, String genre) {
        this.channelName = channelName;
        this.thumb = thumb;
        this.video = video;
        this.genre = genre;
    }
}
