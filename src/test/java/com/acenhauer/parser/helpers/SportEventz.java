package com.acenhauer.parser.helpers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import us.codecraft.xsoup.Xsoup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by guillem on 18/01/16.
 */
public class SportEventz {

    public SportEventz() {
    }

    public ArrayList<Events> sportEventzParsing(String SportEventzHtml,
        ArrayList<Events> listOfEvents, HashMap<String, String> channels, String sportGenre) {
        Document SportEventzFootballDoc = Jsoup.parse(SportEventzHtml);

        Elements numberOfEvents =
            Xsoup.compile("//div[@id='MagicTableContainer']/div/table/tbody/tr")
                .evaluate(SportEventzFootballDoc).getElements();

        int events = numberOfEvents.size();

        System.out.println("Number of events: " + events);
        for (int i = 1; i < events + 1; i++) {
            String eventNameXpath =
                "//div[@id='MagicTableContainer']/div/table/tbody/tr[" + i + "]/td/div/div[2]/h1";
            String eventName = getTextFromXSoupSelect(SportEventzFootballDoc, eventNameXpath);
            String eventTimeXpath =
                "//div[@id='MagicTableContainer']/div/table/tbody/tr[" + i + "]/td/div/div[3]/h3";
            String eventTime = getTextFromXSoupSelect(SportEventzFootballDoc, eventTimeXpath);
            String numberOfChannelsXpath =
                "//div[@id='MagicTableContainer']/div/table/tbody/tr[" + i
                    + "]/td/div/div[@class='MagicTableChannels']/button";
            Elements numberOfChannelsFromEvent =
                Xsoup.compile(numberOfChannelsXpath).evaluate(SportEventzFootballDoc).getElements();

            int numberOfChannels = numberOfChannelsFromEvent.size();

            if (!eventName.isEmpty()) {
                if (checkIfEventIsOnTheList(listOfEvents, eventName, eventTime)) {
                    System.out.println("Adding new channels to an existing event ");
                    for (int k = 0; k < listOfEvents.size(); k++) {
                        String listOfEventsName = listOfEvents.get(k).eventName;
                        if (listOfEventsName.contains("-")) {
                            String[] elements = listOfEventsName.split("-");
                            String[] local = elements[0].split(":");
                            String[] visitor = elements[1].split("\\(");
                            String localTeam = local[2].replace(" ", "").toLowerCase();
                            String visitorTeam = visitor[0].replace(" ", "").toLowerCase();
                            String[] eventFromURL = eventName.split("vs.");
                            String localTeamFromURLToLowerCase =
                                eventFromURL[0].replace(" ", "").toLowerCase();
                            String visitorTeamFromURLToLowerCase =
                                eventFromURL[1].replace(" ", "").toLowerCase();
                            if (localTeamFromURLToLowerCase.contains(localTeam)
                                || visitorTeamFromURLToLowerCase.contains(visitorTeam)) {
                                for (int j = 1; j <= numberOfChannels; j++) {
                                    String originalBroadcasterNameXpath =
                                        "//div[@id='MagicTableContainer']/div/table/tbody/tr[" + i
                                            + "]/td/div/div[@class='MagicTableChannels']/button["
                                            + j + "]/div";
                                    String originalBroadcasterName =
                                        getTextFromXSoupSelect(SportEventzFootballDoc,
                                            originalBroadcasterNameXpath);
                                    for (Map.Entry<String, String> channel : channels.entrySet()) {
                                        String broadcaster =
                                            originalBroadcasterName.replace(" HD", "")
                                                .replace(" hd", "").replace(" ", "").toLowerCase();
                                        String formerChannel =
                                            channel.getKey().replace(" HD", "").replace(" hd", "")
                                                .replace(" ", "").toLowerCase();
                                        if (formerChannel.contains(broadcaster)) {
                                            System.out.println("We have AceStream Link from "
                                                + originalBroadcasterName + "!!");
                                            if (!checkIfChannelsIsAlreadyAdded(
                                                listOfEvents.get(k).channelsInfo,
                                                channel.getKey())) {
                                                listOfEvents.get(k).channelsInfo.add(
                                                    new ChannelInfo(channel.getKey(),
                                                        getLogo(channel.getKey()),
                                                        channel.getValue(),
                                                        getCategory(channel.getKey())));
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    System.out.println("Adding new event");
                    List<ChannelInfo> channelsInfo = new ArrayList<>();
                    String[] newTime = eventTime.split(" ");
                    String year = newTime[3].substring(2);
                    String month = getMonthNumber(newTime[2]);
                    String formerTime[] = newTime[4].split(":");
                    int hourInt = Integer.parseInt(formerTime[0]) + 1;
                    String hour;
                    if (hourInt <= 10) {
                        hour = Integer.toString(hourInt);
                        hour = "0" + hour;
                    } else {
                        hour = Integer.toString(hourInt);
                    }
                    String day = newTime[1];
                    String newEventName =
                        year + "/" + month + "/" + day + " " + hour + ":" + formerTime[1] + " CET ("
                            + sportGenre + "): " + eventName;
                    newEventName = newEventName.replaceAll("vs.", "-").toUpperCase();
                    System.out.println(newEventName);
                    for (int j = 1; j <= numberOfChannels; j++) {
                        String originalBroadcasterNameXpath =
                            "//div[@id='MagicTableContainer']/div/table/tbody/tr[" + i
                                + "]/td/div/div[@class='MagicTableChannels']/button[" + j + "]/div";
                        String originalBroadcasterName =
                            getTextFromXSoupSelect(SportEventzFootballDoc,
                                originalBroadcasterNameXpath);
                        for (Map.Entry<String, String> channel : channels.entrySet()) {
                            String broadcaster =
                                originalBroadcasterName.replace(" HD", "").replace(" hd", "")
                                    .replace(" ", "").toLowerCase();
                            String formerChannel =
                                channel.getKey().replace(" HD", "").replace(" hd", "")
                                    .replace(" ", "").toLowerCase();
                            if (formerChannel.contains(broadcaster)) {
                                System.out.println(
                                    "We have AceStream Link from " + originalBroadcasterName
                                        + "!!");
                                if (!checkIfChannelsIsAlreadyAdded(channelsInfo,
                                    channel.getKey())) {
                                    channelsInfo.add(
                                        new ChannelInfo(channel.getKey(), getLogo(channel.getKey()),
                                            channel.getValue(), getCategory(channel.getKey())));
                                }
                            }
                        }
                    }
                    if (!channelsInfo.isEmpty()) {
                        listOfEvents.add(new Events(newEventName.toUpperCase(), channelsInfo));
                    }
                }
            }
        }
        return listOfEvents;
    }

    private boolean checkIfEventIsOnTheList(ArrayList<Events> listOfEvents, String eventName,
        String eventTime) {
        for (int k = 0; k < listOfEvents.size(); k++) {
            String listOfEventsName = listOfEvents.get(k).eventName;
            if (!listOfEventsName.contains("ARTES MARCIALES MIXTAS") && !listOfEventsName
                .contains("TENIS") && !listOfEventsName.contains("SIMULCAST")) {
                listOfEventsName = listOfEventsName.toLowerCase();
                String[] elements = listOfEvents.get(k).eventName.split("-");
                String[] local = elements[0].split(":");
                String[] visitor = elements[1].split("\\(");
                String localTeam = local[2].replace(" ", "").toLowerCase();
                String visitorTeam = visitor[0].replace(" ", "").toLowerCase();
                String[] eventFromURL = eventName.split("vs.");
                String localTeamFromURLToLowerCase = eventFromURL[0].replace(" ", "").toLowerCase();
                String visitorTeamFromURLToLowerCase =
                    eventFromURL[1].replace(" ", "").toLowerCase();
                if (localTeamFromURLToLowerCase.contains(localTeam) || visitorTeamFromURLToLowerCase
                    .contains(visitorTeam)) {
                    String[] newTime = eventTime.split(" ");
                    String year = newTime[3].substring(2);
                    String month = getMonthNumber(newTime[2]);
                    String day = newTime[1];
                    if (listOfEventsName.contains(month) && listOfEventsName.contains(day)
                        && listOfEventsName.contains(year)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public String getMonthNumber(String NombreMes) {

        String NumeroMes;

        switch (NombreMes.toLowerCase()) {

            case ("january"):
                NumeroMes = "01";
                return NumeroMes;

            case ("february"):
                NumeroMes = "02";
                return NumeroMes;

            case ("march"):
                NumeroMes = "03";
                return NumeroMes;

            case ("april"):
                NumeroMes = "04";
                return NumeroMes;

            case ("may"):
                NumeroMes = "05";
                return NumeroMes;

            case ("june"):
                NumeroMes = "06";
                return NumeroMes;

            case ("july"):
                NumeroMes = "07";
                return NumeroMes;

            case ("august"):
                NumeroMes = "08";
                return NumeroMes;

            case ("september"):
                NumeroMes = "09";
                return NumeroMes;

            case ("october"):
                NumeroMes = "10";
                return NumeroMes;

            case ("november"):
                NumeroMes = "11";
                return NumeroMes;

            case ("december"):
                NumeroMes = "12";
                return NumeroMes;

            default:
                return "ERROR";
        }
    }

    public String getTextFromXSoupSelect(Document doc, String html) {
        try {
            return Jsoup.parse(Xsoup.select(doc, html).get()).body().text();
        } catch (IllegalArgumentException e) {
            return "";
        }
    }

    public boolean checkIfChannelsIsAlreadyAdded(List<ChannelInfo> channelsList, String channel) {
        for (ChannelInfo channelElement : channelsList) {
            String broadcaster =
                channelElement.channelName.replace(" HD", "").replace(" hd", "").replace(" ", "")
                    .toLowerCase();
            String formerChannel =
                channel.replace(" HD", "").replace(" hd", "").replace(" ", "").toLowerCase();
            if (formerChannel.contains(broadcaster)) {
                return true;
            }
        }
        return false;
    }

    public String getCategory(String key) {
        if (key.contains("Arena Vision")) {
            return "Arena Vision";
        } else if (key.contains("Acefootball")) {
            return "Acefootball";
        } else if (key.contains("Al Jazeera Sport")) {
            return "Al Jazeera Sport";
        } else if (key.contains("Arena Sport")) {
            return "Arena Sport";
        } else if (key.contains("Arenavision")) {
            return "Arenavision";
        } else if (key.contains("AzTV")) {
            return "AzTV";
        } else if (key.contains("BBC")) {
            return "BBC";
        } else if (key.contains("BeIN Sport")) {
            return "BeIN Sport";
        } else if (key.contains("Benfica TV")) {
            return "Benfica TV";
        } else if (key.contains("BT Sport")) {
            return "BT Sport";
        } else if (key.contains("C+") || key.contains("Canal+")) {
            return "Canal+";
        } else if (key.contains("Digi Sport")) {
            return "Digi Sport";
        } else if (key.contains("Eurosport")) {
            return "Eurosport";
        } else if (key.contains("ESPN")) {
            return "ESPN";
        } else if (key.contains("Fox Sports")) {
            return "Fox Sports";
        } else if (key.contains("NTV Plus")) {
            return "NTV Plus";
        } else if (key.contains("Sky Sports")) {
            return "Sky Sports";
        } else if (key.contains("TSN")) {
            return "TSN";
        } else if (key.contains("Viasat")) {
            return "Viasat";
        }
        return "Sports";
    }

    public String getLogo(String key) {
        if (key.contains("Arena Vision")) {
            return "http://acenhauer.com/logos/arenavision.png";
        } else if (key.contains("Acefootball")) {
            return "http://acenhauer.com/logos/acestream.png";
        } else if (key.contains("Al Jazeera Sport")) {
            return "http://acenhauer.com/logos/aljazeera.jpg";
        } else if (key.contains("Arena Sport")) {
            return "http://acenhauer.com/logos/arenasport.jpg";
        } else if (key.contains("Arenavision")) {
            return "http://acenhauer.com/logos/arenavision.png";
        } else if (key.contains("AzTV")) {
            return "http://acenhauer.com/logos/aztv.jpg";
        } else if (key.contains("BBC")) {
            return "http://acenhauer.com/logos/bbc.png";
        } else if (key.contains("BeIN Sport")) {
            return "http://acenhauer.com/logos/bein.gif";
        } else if (key.contains("Benfica TV")) {
            return "http://acenhauer.com/logos/benfica.png";
        } else if (key.contains("BT Sport")) {
            return "http://acenhauer.com/logos/btsport.png";
        } else if (key.contains("C+") || key.contains("Canal+")) {
            return "http://acenhauer.com/logos/Canal+.jpg";
        } else if (key.contains("Digi Sport")) {
            return "http://acenhauer.com/logos/digisport.png";
        } else if (key.contains("Eurosport")) {
            return "http://acenhauer.com/logos/eurosport.jpg";
        } else if (key.contains("ESPN")) {
            return "http://acenhauer.com/logos/espn.gif";
        } else if (key.contains("Fox Sports")) {
            return "http://acenhauer.com/logos/foxsports.png";
        } else if (key.contains("NTV Plus")) {
            return "http://acenhauer.com/logos/ntv.jpg";
        } else if (key.contains("Sky Sports")) {
            return "http://acenhauer.com/logos/skysports.jpg";
        } else if (key.contains("TSN")) {
            return "http://acenhauer.com/logos/tsn.jpg";
        } else if (key.contains("Viasat")) {
            return "http://acenhauer.com/logos/viasat.png";
        }
        return "http://acenhauer.com/logos/acestream.png";
    }

    public boolean checkIfCannelIsInTheList(HashMap<String, String> channels, String channel) {
        for (Map.Entry<String, String> entry : channels.entrySet()) {
            String channelFromList = entry.getKey().toLowerCase().replace(" ", "");
            String channelToCompare = channel.toLowerCase().replace(" ", "");
            if (channelFromList.contains(channelToCompare)) {
                System.out
                    .println("Channel from list: " + channelFromList + " vs " + channelToCompare);
                return true;
            }
        }
        return false;
    }
}
