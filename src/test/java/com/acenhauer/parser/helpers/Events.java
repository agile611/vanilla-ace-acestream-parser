package com.acenhauer.parser.helpers;

import java.util.List;

/**
 * Created by guillem on 15/01/16.
 */
public class Events {

    public String eventName;
    public List<ChannelInfo> channelsInfo;

    public Events(String eventName, List<ChannelInfo> channelsInfo) {
        this.eventName = eventName;
        this.channelsInfo = channelsInfo;
    }

}

