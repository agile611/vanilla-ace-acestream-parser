Vanilla Ace Acestream Parser
===============

Vanilla Ace Acestream parser to provide channel feeds for the Kodi Addon.
The author does not host or distribute any of the content displayed by this addon and additionally does not have any affiliation with the content provider.
